package ru.fedun.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.ISessionService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.*;
import ru.fedun.tm.exception.empty.EmptyCommandException;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.service.CommandService;
import ru.fedun.tm.service.SessionService;
import ru.fedun.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final DataEndpointService dataEndpointService = new DataEndpointService();

    @NotNull
    private final DataEndpoint dataEndpoint = dataEndpointService.getDataEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.fedun.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.fedun.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    @SneakyThrows
    public void run(final String[] args) {
        init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            runWithCommand(TerminalUtil.nextLine());
        }
    }

    @SneakyThrows
    private void runWithCommand(@NotNull final String cmd) {
        if (cmd.isEmpty()) throw new EmptyCommandException();
        @NotNull final AbstractCommand command = commands.get(cmd);
        command.execute();
    }

    @SneakyThrows
    public boolean parseArgs(@NotNull final String[] args) {
        if (args.length == 0) return false;
        @NotNull final String arg = args[0];
        runWithArg(arg);
        return true;
    }

    @SneakyThrows
    private void runWithArg(@NotNull final String arg) {
        if (arg.isEmpty()) return;
        @NotNull final AbstractCommand argument = arguments.get(arg);
        argument.execute();
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public DataEndpoint getDataEndpoint() {
        return dataEndpoint;
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return adminEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}
