package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@NotNull String login, @NotNull String password) throws Exception;

    boolean isValid(@NotNull Session session);

    void validate(@NotNull Session session) throws Exception;

    void validate(@NotNull Session session, @NotNull Role role) throws Exception;

    @NotNull
    Session open(@NotNull String login, @NotNull String password) throws Exception;

    @Nullable
    Session sign(@NotNull Session session);

    @NotNull
    User getUser(@NotNull Session session) throws Exception;

    @Nullable
    String getUserId(@NotNull Session session) throws Exception;

    @NotNull
    List<Session> getSessionList(@NotNull Session session) throws Exception;

    void close(@NotNull Session session) throws Exception;

    void closeAll(@NotNull Session session) throws Exception;

    void signOutByLogin(@NotNull String login) throws Exception;

    void signOutByUserId(@NotNull String userId) throws Exception;

}

