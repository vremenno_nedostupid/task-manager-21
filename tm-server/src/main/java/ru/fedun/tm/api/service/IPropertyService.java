package ru.fedun.tm.api.service;

public interface IPropertyService {

    void init() throws Exception;

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

}
