package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.Task;

import java.util.List;

public interface ITaskEndpoint {

    void createTask(
            @NotNull final Session session,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception;

    void clearTasks(@NotNull final Session session) throws Exception;

    @NotNull List<Task> showAllTasks(@NotNull final Session session) throws Exception;

    @NotNull Task showTaskById(@NotNull final Session session, @NotNull final String id) throws Exception;

    @NotNull Task showTaskByIndex(@NotNull final Session session, @NotNull final Integer index) throws Exception;

    @NotNull Task showTaskByName(@NotNull final Session session, @NotNull final String name) throws Exception;

    @NotNull Task updateTaskById(
            @NotNull final Session session,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception;

    @NotNull Task updateTaskByIndex(
            @NotNull final Session session,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception;

    @NotNull Task removeTaskById(@NotNull final Session session, @NotNull final String id) throws Exception;

    @NotNull Task removeTaskByIndex(@NotNull final Session session, @NotNull final Integer index) throws Exception;

    @NotNull Task removeTaskByName(@NotNull final Session session, @NotNull final String name) throws Exception;

}
