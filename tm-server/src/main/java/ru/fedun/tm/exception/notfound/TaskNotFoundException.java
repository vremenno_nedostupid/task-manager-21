package ru.fedun.tm.exception.notfound;

import ru.fedun.tm.exception.AbstractRuntimeException;

public final class TaskNotFoundException extends AbstractRuntimeException {

    private final static String message = "Error! Task not found...";

    public TaskNotFoundException() {
        super(message);
    }

}
